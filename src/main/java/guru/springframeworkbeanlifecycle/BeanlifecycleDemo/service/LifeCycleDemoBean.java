package guru.springframeworkbeanlifecycle.BeanlifecycleDemo.service;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class LifeCycleDemoBean implements InitializingBean,DisposableBean,BeanNameAware,BeanFactoryAware,ApplicationContextAware
{
    public LifeCycleDemoBean()
    {
        System.out.println("I am in constructor");
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException
    {
        System.out.println(" beanfactory has set");

    }

    @Override
    public void setBeanName(String name)
    {
    System.out.println("My bean name is"+name);
    }

    @Override
    public void destroy() throws Exception
    {
  System.out.println("lifecycle is destroyed");
    }

    @Override
    public void afterPropertiesSet() throws Exception
    {
   System.out.println("lifecycle hads its propertes set");
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException
    {
    System.out.println("ApplicationContext has set");
    }

    @PostConstruct
    public void postConstruct()
    {
        System.out.println("postConstruct annotaed method is called");
    }
    @PreDestroy
    public void preDestroy()
        {
            System.out.println("Predestroy annotation method  is called");
        }

        public void beforeInit()
        {
System.out.println("beforeInit called by beanpostProcessor");
        }

        public void afterInit()
        {
System.out.println("afterInit called by beanpostProceesor");
        }



}
