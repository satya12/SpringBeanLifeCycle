package guru.springframeworkbeanlifecycle.BeanlifecycleDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BeanlifecycleDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(BeanlifecycleDemoApplication.class, args);
	}
}
